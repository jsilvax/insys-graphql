declare module "*.png";
declare module "*.ico";
declare module "*.jpg";
declare module "*.jpeg";
declare module "*.svg";
declare module "*.gif";
declare module "*.css";
declare module "*.scss";

declare module '*.gql' {
  import {DocumentNode} from 'graphql';

  const value: DocumentNode;
  export = value;
}

declare namespace NodeJS {
  interface Global {
    logIn : any;
    logOut : any;
    logInAdmin : any;
    logInUser : any;
    server : any;
    request : any;
  }
}

declare interface User {
  _id: String;
  role: String;
}

declare interface Context {
  isAuthenticated: boolean;
  user: User;
}
