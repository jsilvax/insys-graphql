# INSYS GRAPHQL API
The GraphQL API for insys

## Getting Started
```
yarn install
```
then build the server bundle: 
```
yarn build
```
and start up the application:
```
yarn dev
```
## Tests
insys-graphql does integration as well as unit tests. Integration hits the endpoint directly using supertest. The Unit tests mock. 

`NOTE: Make sure mongo is running:`
```
mongod
```
#### Integration

```
yarn integration
```
#### Unit

```
yarn test
```

## WebFaction Helper

SSH into the webapp & CD into /insys_graphql/ then export the path

```
export PATH=$PWD/bin/:$PATH
```