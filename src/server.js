import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import resolvers from './api/resolvers';
import mongoose from 'mongoose';
import cookieParser from 'cookie-parser';
import { MONGO_DB, JWT } from './config/config';
import { isAuthHeaderPresent, Authenticate } from './api/auth/auth';
import typeDefs from './api/typeDefs';
import setUpQueryCache from './helpers/setUpQueryCache';
import bodyParser from 'body-parser';
import morgan from '@/logging/morgan';
import logger from '@/logging/logger';

// Create the express app & export
export const app = express();

// Set the logger stream
app.use(morgan({ stream: logger.stream }));

// cookie middleware to have cookies available in req.signedCookies['my-cookie-name']
app.use(cookieParser(JWT));

// parse application/json
app.use(bodyParser.json());

// Connect to DB
mongoose.connect(
	MONGO_DB,
	{ useNewUrlParser: true, useCreateIndex: true }
);

// Create the Apollo Server
export const server = new ApolloServer({
	typeDefs,
	resolvers,
	persistedQueries: setUpQueryCache(),
	context({ req, res }) {
		// Some resolvers like (logIn) need to be open, so unauth errors will happen per resolvers
		if (isAuthHeaderPresent(req)) return Authenticate(req, res);
		return { req, res, isAuthenticated: false };
	},
	introspection: true, // playground options for prod
	playground: {
		settings: {
			'request.credentials': 'same-origin'
		}
	},
	tracing: true
});

server.applyMiddleware({ app });
