export const NODE_ENV = process.env.NODE_ENV || 'development';
export const IS_PROD = process.env.NODE_ENV === 'production';
export const PORT = process.env.PORT || 9999;
export const LOG_LEVEL = process.env.TEST_TYPE ? 'warn' : process.env.LOG_LEVEL || 'warn';

// MONGO
const TEST_DB = process.env.TEST_TYPE ? 'mongodb://localhost/insys-int' : null;
export const MONGO_DB = process.env.MONGO_DB || TEST_DB || 'mongodb://localhost/insys';

// AUTH
export const JWT = process.env.JWT || 'scooby';
export const JWT_EXPIRE_TIME = 24 * 60 * 10; // 10 days in minutes
export const JWT_FORGOTTEN = process.env.JWT_FORGOTTEN || 'scrappy';

// CACHE
export const QUERY_CACHE_TTL = parseInt(process.env.QUERY_CACHE_TTL) || 3600;
export const REDIS_URL = process.env.REDIS_URL;
export const REDIS_PASSWORD = process.env.REDIS_PASSWORD;
