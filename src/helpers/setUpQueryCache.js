import { RedisCache } from 'apollo-server-cache-redis';
import { IS_PROD, QUERY_CACHE_TTL, REDIS_URL, REDIS_PASSWORD } from '../config/config';

/**
 * @function setUpQueryCache
 * @description Get options for query cache setup
 * @return {Object|Boolean}
 */
const setUpQueryCache = () => {
	return false;
	if (!IS_PROD) return false;

	const cache = new RedisCache({
		url: REDIS_URL,
		enable_offline_queue: false,
		password: REDIS_PASSWORD
	});
	/* RedisCache does't use this.options, they use defaultSetOptions
	  Which sucks, but this is the only way to update the TTL */
	cache.defaultSetOptions.ttl = QUERY_CACHE_TTL;

	return { cache };
};

export default setUpQueryCache;
