import serialize from 'serialize-javascript';

/**
 * @function serialize
 * @description serialize all fields
 * @param {Object} doc - The document object to serialize
 * @return {Object} A clean document
 */
export default doc => {
	const clean = {};
	Object.entries(doc).forEach(([key, value]) => {
		if (key === 'user' || key === '_id') {
			clean[key] = value;
		} else {
			clean[key] = serialize(value);
		}
	});
	return clean;
};
