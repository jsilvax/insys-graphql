import Company from './company.model';
import { AuthenticateResolver, AuthenticateSameUser } from '../auth/auth';

/**
 * @function companies
 * @description GET companies
 * @param {Object} root
 * @param {Object} args
 * @param {Object} context
 * @param {Object} info
 * @return {Promise<Array|null>}
 */
const companies = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Company.paginate({ user: context.user._id })
		.then(companies => companies)
		.catch(err => {
			console.error('Companies Error', err);
			return null;
		});
};

/**
 * @function company
 * @description GET a single company
 * @param {Object} root
 * @param {Object} args
 * @param {String} args._id
 * @param {Object} context
 * @param {Object} info
 * @return {Promise<Object|null>}
 */
const company = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Company.findById(_id)
		.then(company => {
			if (!company) throw new Error('No company with that id');
			AuthenticateSameUser(context, { _id: company.user });
			return company;
		})
		.catch(err => {
			console.error('Company Error', err);
			return null;
		});
};

export default {
	companies,
	company
};
