import Query from './company.query';
import Mutation from './company.mutation';
import { user } from '../users/users';

export default {
	Query,
	Mutation,
	Company: {
		user
	}
};
