import Company from './company.model';
import { AuthenticateResolver, isAdmin, AuthenticateSameUser } from '../auth/auth';

/**
 * @function createCompany
 * @description POST Create a new company document in mongo
 * @param {Object} root
 * @param {Object} args
 * @param {Object} context
 * @param {Object} info
 * @return {Promise<Object>}
 */
const createCompany = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	const newCompany = args.company;
	// Make sure we only save to the user logged in
	const user =
		isAdmin(context.user.role) && args.company.user ? args.company.user : context.user._id;
	newCompany.user = user;

	return Company.create(newCompany)
		.then(company => company)
		.catch(err => {
			console.error('Error creating new Company', err);
			throw err;
		});
};

/**
 * @function updateCompany
 * @description PUT Update a company document in mongo
 * @param {Object} root
 * @param {Object} args - The arguments passed to the query or mutation
 * @param {Object} args.company
 * @param {String} args.company._id
 * @param {Object} context
 * @param {Object} info
 * @return {Promise<Object>}
 */
const updateCompany = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Company.findById(args.company._id)
		.then(company => {
			// Make sure it's the same user, will throw if not
			AuthenticateSameUser(context, { _id: company.user });
			// Make sure we only save to the user logged in
			const user = isAdmin(context.user.role) ? company.user : context.user._id;
			const update = { ...args.company, user };
			Object.assign(company, update);
			return company.save();
		})
		.catch(err => {
			console.log('Problem Updating Company', err);
			throw err;
		});
};

/**
 * @function deleteCompany
 * @description DELETE deletes a company
 * @param {Object} root - the root resolver object passed (None here since this IS the root)
 * @param {Object} args
 * @param {Object} context
 * @param {Object} info
 * @return {Promise<Object>}
 */
const deleteCompany = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Company.findById(_id)
		.then(company => {
			// Make sure it's the same user, will throw if not
			AuthenticateSameUser(context, {
				_id: company.user
			});
			return company;
		})
		.catch(err => {
			console.error('Error deleting company', err);
			throw err;
		});
};

export default {
	createCompany,
	deleteCompany,
	updateCompany
};
