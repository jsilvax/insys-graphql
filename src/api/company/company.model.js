import mongoose from 'mongoose';
const Schema = mongoose.Schema;

// downloadPath - The download path for the invoices

const CompanySchema = new Schema({
	name: {
		type: String,
		required: [true, 'Company must have a name']
	},
	license: {
		type: String
	},
	phone: {
		type: String
	},
	email: {
		type: String
	},
	address: {
		type: String
	},
	city: {
		type: String
	},
	state: {
		type: String
	},
	zip: {
		type: String
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'user', // the "user" model defined in user-model.js
		required: true
	},
	downloadPath: {
		type: String
	}
});

CompanySchema.methods = {
	toJson: function() {
		// mongoose returns a document that looks like an object but isnt,
		// this will just convert it so that it is an object
		return this.toObject();
	}
};

export default mongoose.model('company', CompanySchema);
