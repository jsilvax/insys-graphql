afterEach(done => {
	logOut().then(() => {
		done();
	});
});

describe('Test Log out Functionality', () => {
	it('should log out', () => {
		return logInUser()
			.then(() => logOut())
			.then(({ message }) => {
				expect(message).toEqual('Successfully logged out');
			});
	});
});

describe('Basic Log In', () => {
	it('should be able to log in when username and password are correct', () => {
		return logInAdmin().then(({ token, user }) => {
			expect(typeof token).toEqual('string');
			expect(user.username).toEqual('jsilvax');
			expect(user.role).toEqual('admin');
		});
	});

	it('should NOT be able to log in with incorrect password', () => {
		return logIn('jsilvax', 'woo').catch(err => {
			expect(err[0].message).toEqual('Error: Incorrect password');
		});
	});

	it('should NOT be able to log in with incorrect username', () => {
		return logIn('hello', 'password').catch(err => {
			expect(err[0].message).toEqual('Error: No user with that username');
		});
	});
});
