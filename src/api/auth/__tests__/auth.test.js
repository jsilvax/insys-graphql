import { isAuthHeaderPresent, verifyClientToken } from '../auth';

describe('isAuthHeaderPresent', () => {
	let req = {};
	beforeEach(() => {
		req = {};
		req.headers = {};
		req.query = {};
	});

	it('should return truthy if a token is present on the query param', () => {
		const expected = 'eyJhbGciOiJIUzI1NiIsIn';
		req.query = {
			access_token: expected
		};
		expect(isAuthHeaderPresent(req)).toBeTruthy();
		expect(req.headers.authorization).toEqual(`Bearer ${expected}`);
	});

	it('should return truthy if a token is present in the headers authorization', () => {
		const expected = 'Bearer eyJhbGciOiJIUzI1NiIsIn';
		req.headers = { authorization: expected };
		expect(isAuthHeaderPresent(req)).toBeTruthy();
	});

	it('should return false when query and headers are missing', () => {
		expect(isAuthHeaderPresent(req)).toBeFalsy();
	});
});

describe('verifyClientToken', () => {
	let req = {};
	beforeEach(() => {
		req = {};
		req.signedCookies = {};
		req.headers = {};
	});

	it('should resolve when client token matches server token', done => {
		const expected = 'eyJhbGciOiJIUzI1NiIsIn';
		// The server cookie
		req.signedCookies['insys-token'] = expected;
		// Client token
		req.headers.authorization = `Bearer ${expected}`;
		verifyClientToken(req, {}).then(request => {
			expect(request).toBeDefined();
			done();
		});
	});

	it('should throw when client token is different from server token', done => {
		const expected = 'eyJhbGciOiJIUzI1NiIsIn';
		// The server cookie
		req.signedCookies['insys-token'] = expected;
		// Client token
		req.headers.authorization = `Bearer uay_SewYSw`;
		verifyClientToken(req, {}).catch(err => {
			expect(err.message).toEqual(expect.stringContaining('Unauthorized Token'));
			done();
		});
	});
});
