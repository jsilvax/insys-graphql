import jwt from 'jsonwebtoken';
import User from '../users/users.model';
import { JWT, JWT_EXPIRE_TIME, JWT_FORGOTTEN } from '../../config/config';
import logger from '@/logging/logger';

/**
 * @function signToken
 * @param {String} _id
 * @return {String}
 */
const signToken = _id => {
	return jwt.sign({ _id }, JWT, {
		expiresIn: JWT_EXPIRE_TIME
	});
};

/**
 * @function signForgottenToken
 * @param {String} _id
 * @return {String}
 */
const signForgottenToken = _id => {
	return jwt.sign({ _id }, JWT_FORGOTTEN, {
		expiresIn: '24h'
	});
};

/**
 * @function authenticatePassword
 * @param {Object} user The mongo user model / object
 * @param {String} password The provided password
 * @return {Promise<Object>}
 */
const authenticatePassword = async (user, password) => {
	if (!user) throw new Error('No user with that username');
	if (!user.authenticate(password)) throw new Error('Incorrect password');
	return user;
};

/**
 * @function logIn
 * @param {Object} root
 * @param {Object} args
 * @param {String} args.username
 * @param {String} args.password
 * @return {Promise<Object>}
 */
const logIn = (root, { username, password }, { res, req }) => {
	return User.findOne({ username })
		.then(user => authenticatePassword(user, password))
		.then(user => {
			const token = signToken(user._id);
			const { username, role } = user;
			// Set token
			res.cookie('insys-token', token, {
				sameSite: true,
				httpOnly: true,
				signed: true,
				maxAge: 1000 * 60 * 60 * 24 * 365 // 1 yr
			});
			logger.info('Successfully logged in %s', JSON.stringify({ username, role }));
			return { token, user: user.toJson() };
		})
		.catch(err => {
			logger.error('Error in logIn', err);
			throw new Error(err);
		});
};

/**
 * @function logOut
 * @description Logs a user out and removes the cookie
 * @return {Object}
 */
const logOut = (root, args, { res }) => {
	res.clearCookie('insys-token');
	return { message: 'Successfully logged out' };
};

/**
 * @function forgottenPassword
 * @description Allows users to request a token to change their password
 * @param {Object} root
 * @param {Object} args
 * @param {String} args.username
 * @return {Promise<Object>}
 */
const forgottenPassword = (root, { username }) => {
	return User.findOne({ username })
		.then(({ email, _id }) => {
			if (!email) throw new Error('No email associated with that username');
			const token = signForgottenToken(_id);
			// TODO: Step 2. Send email with link to forgotten password screen
			console.log('email', email);
			console.log('token', token);
			return { message: 'Sent an email with reset instructions' };
		})
		.catch(err => {
			logger.error('Error in forgotten password', err);
			throw new Error(err);
		});
};

export default {
	Mutation: {
		logIn,
		logOut,
		forgottenPassword
	}
};
