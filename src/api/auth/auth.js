import expressJwt from 'express-jwt';
import { JWT } from '../../config/config';
import { getUserById } from '../../api/users/users';
import logger from '@/logging/logger';

/**
 * @function isAuthHeaderPresent
 * @description Verifies that there even is an auth header
 * @param {Object} req
 * @return {Boolean}
 */
export const isAuthHeaderPresent = req => {
	// This gives the option to get the access token through a ? param.
	// Alternatively the request can come in the actual req.headers.authorization
	if (req.query && req.query.hasOwnProperty('access_token')) {
		// Bearer is part of the spec that JWT uses. Bearer 38383939 format
		req.headers.authorization = `Bearer ${req.query.access_token}`;
	}
	return typeof req.headers.authorization === 'string';
};

/**
 * @function verifyClientToken
 * @description Verifies that the token being used on the clientside is the same as the server.
 * This ensures that the clientside token being used wasn't stolen from a diffrent user
 */
export const verifyClientToken = function(req, res) {
	return new Promise((resolve, reject) => {
		if (req.headers.authorization === 'Bearer ' + req.signedCookies['insys-token']) {
			return resolve(req, res);
		}
		logger.error('Auth token in server is', req.signedCookies['insys-token']);
		return reject(new Error('Unauthorized Token - Server token is not the same or missing'));
	});
};

/**
 * @function checkToken
 * @description Returns a middleware function from express-jwt
 */
const checkToken = expressJwt({ secret: JWT });

/**
 * @function decodeToken
 * @description Decodes token based on auth headers. Attaches valid token to req.user
 * @param {Object} req
 * @param {Object} res
 * @param {Function} [checkTokenMW] The checkToken middleware function
 */
export const decodeToken = (req, res, checkTokenMW = checkToken) => {
	return new Promise((resolve, reject) => {
		const next = err => {
			if (err) return reject(err);
			resolve();
		};
		// This will call the next func & resolve if the token is valid
		// and will send error if its not.
		// The error will be catched by the context func in our middleware (server.js)
		// If valid it will attach the decoded token to req.user
		checkTokenMW(req, res, next);
	});
};

/**
 * @function Authenticate
 * @description The main starting point for authenticating
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<Object>}
 */
export const Authenticate = (req, res) => {
	return verifyClientToken(req, res)
		.then(decodeToken)
		.then(() => getUserById(req.user._id))
		.then(user => {
			logger.info('isAuthenticated', user);
			return { isAuthenticated: true, user, req, res };
		})
		.catch(err => {
			logger.error(err);
			// Context expects the promise to resolve. We'll reject at the resolver level.
			return {
				isAuthenticated: false,
				error: err,
				req,
				res
			};
		});
};

/**
 * @function isAdmin
 * @description A helper function to determine if a user is an admin role
 * @param {String} role
 * @return {Boolean}
 */
export const isAdmin = role => role === 'admin';

/**
 * @function AuthenticateSameUser
 * @param {Object} context passed down from resolver. Defined in server.js
 * @param {Boolean} context.isAuthenticated
 * @param {Object} context.user
 * @param {String} context.user._id
 * @param {String} context.user.role
 * @param {Object} opts
 * @param {String} opts._id The _id of a param ex. (user | customer) Used to verify same user.
 */
export const AuthenticateSameUser = (context, opts = { _id: null }) => {
	// Check if its the same user making the request
	// Normalize the objects, since they're not considered 100% the same objects (even though theyre the same id)  / the id param would never be === to the actual user id since it is placed in the user obj
	if (
		opts._id &&
		context.user._id.toString() !== opts._id.toString() &&
		context.user.role !== 'admin'
	) {
		throw Error('Unauthorized - You can only access your items');
	}
};
/**
 * @function AuthenticateResolver
 * @param {Object} context passed down from resolver. Defined in server.js
 * @param {Boolean} context.isAuthenticated
 * @param {String} [context.error]
 * @param {Object} context.user
 * @param {String} context.user._id
 * @param {String} context.user.role
 * @param {Object} [opts]
 * @param {Boolean} [opts.isAdminOnly] A "resolver" that's protected and only admin's are allowed
 * @param {String} [opts._id] The _id of a param ex. (user | customer) Used to verify same user.
 */
export const AuthenticateResolver = (context, opts = { isAdminOnly: false, _id: null }) => {
	if (!context.isAuthenticated) {
		logger.error('Unauthorized', context.error);
		throw new Error(context.error || 'Unauthorized, please log in');
	}
	if (opts.isAdminOnly && !isAdmin(context.user.role)) {
		throw Error('Unauthorized - You are not an admin');
	}
	// Only check if they provided an id / check if its the same user
	opts._id && AuthenticateSameUser(context, { _id: opts._id });
};
