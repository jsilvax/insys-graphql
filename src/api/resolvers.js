import merge from 'lodash.merge';
import authResolvers from './auth/auth.resolvers';
import companyResolvers from './company/company.resolvers';
import customersResolvers from './customers/customers.resolvers';
import invoiceResolvers from './invoice/invoice.resolvers';
import usersResolvers from './users/users.resolvers';
import propertiesResolvers from './properties/properties.resolvers';

export default merge(
	{},
	authResolvers,
	companyResolvers,
	customersResolvers,
	invoiceResolvers,
	usersResolvers,
	propertiesResolvers
);
