import {
	allUsers,
	getUserWithRoleUser,
	createUser,
	updateUserToAdmin,
	deleteUser
} from './__fixtures__/users.queries';

afterEach(done => {
	logOut().then(() => {
		done();
	});
});

describe('GET ALL users', () => {
	it('should get ALL users if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(allUsers)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { users } = body.data;
				expect(users.length).toBeGreaterThan(0);
				done();
			});
	});

	it('should NOT get ALL users if UNAUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(allUsers)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized - You are not an admin');
				done();
			});
	});
});

describe('GET user', () => {
	it('should get user if AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(getUserWithRoleUser)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				expect(body.data.user.username).toEqual('user1');
				done();
			});
	});
	it('should get user if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(getUserWithRoleUser)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				expect(body.data.user.username).toEqual('user1');
				done();
			});
	});
});

describe('POST user', () => {
	it('should NOT create a user if UNAUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(createUser)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized - You are not an admin');
				done();
			});
	});

	it('should create user if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(createUser)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { createUser } = body.data;
				expect(createUser.username).toEqual('khaleesi');
				expect(createUser.role).toEqual('admin');
				done();
			});
	});
});

describe('UPDATE user', () => {
	it('should NOT update user if UNAUTHORIZED (user1)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(updateUserToAdmin())
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized - You can only access your items');
				done();
			});
	});

	it('should update user but not role if AUTHORIZED (user2)', async done => {
		const { cookie, token } = await logInUserTwo();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(updateUserToAdmin())
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { updateUser } = body.data;
				expect(updateUser.email).toEqual('test@test.com');
				expect(updateUser.role).toEqual('user');
				done();
			});
	});

	it('should update user and role if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(updateUserToAdmin('woop@woop.com'))
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { updateUser } = body.data;
				expect(updateUser.email).toEqual('woop@woop.com');
				expect(updateUser.role).toEqual('admin');
				done();
			});
	});
});

describe('DELETE user', () => {
	it('should NOT delete user if UNAUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(deleteUser)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized - You are not an admin');
				done();
			});
	});

	it.skip('should delete user if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(deleteUser)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { deleteUser } = body.data;
				expect(deleteUser.username).toEqual('user1');
				done();
			});
	});
});
