const admin = '5c93eb7cc36fa055bcd89e1e';
const user1 = '5c93ebe8287df955cec0090e';
const user2 = '5c93ebf8f1b98355dad27045';

export const allUsers = {
	query: `
		query ALL_USERS {
			users {
				_id
				username
				role
			}
		}
	`
};

export const getUserWithRoleUser = {
	query: `
		query GET_USER {
			user(_id: "${user1}") {
				_id
				username
				role
			}
		}
	`
};

export const createUser = {
	query: `
		mutation CREATE_USER {
			createUser(user: {
				username : "khaleesi", 
				password: "motherofdragons",
				role : "admin"
			}) {
				_id
				username
				role
			}
		}
	`
};

export const updateUserToAdmin = (email = 'test@test.com') => ({
	query: `
		mutation UPDATE_USER {
			updateUser(user: {
				_id : "${user2}"
				role : "admin", 
				email : "${email}"
			}) {
				_id
				username
				email
				role
			}
		}
	`
});

export const deleteUser = {
	query: `
		mutation DELETE_USER {
			deleteUser(_id: "${user1}") {
				_id
				username
				role
			}
		}
	`
};
