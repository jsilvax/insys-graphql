import Users from "./users.model";

/**
 * @function getUserById
 * @description The interface resolver helper func
 * @param {String} _id
 * @return {Promise<Object>}
 */
export const getUserById = _id => {
  return Users.findById(_id)
    .select("-password")
    .then(user => {
      if (!user) throw new Error("No User with that id");
      return user;
    });
};

/**
 * @function user
 * @description The interface resolver helper func
 * @param {Object} root - The root object passed from the resolver
 * @param {String} root.user - The user _id
 * @return {Promise<Object>}
 */

export const user = root => {
  return getUserById(root.user).catch(() => null);
};
