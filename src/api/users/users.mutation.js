import Users from './users.model';
import { isAdmin, AuthenticateResolver, AuthenticateSameUser } from '../auth/auth';

const getDocumentUser = (context, model) => {
	return isAdmin(context.user.role) && model.user ? model.user : context.user._id;
};

/**
 * @function createUser
 * @description POST Create a new user document in mongo
 * @return {Promise<Object>}
 */
const createUser = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context, { isAdminOnly: true });
	// Make sure we only save to the owner (user)
	const usr = getDocumentUser(context, args.user);

	const newUser = {
		...args.user,
		user: usr
	};
	return Users.create(newUser)
		.then(user => user)
		.catch(err => {
			console.error('Error creating new Users', err);
			throw err;
		});
};

/**
 * @function updateUser
 * @description PUT Update a user document in mongo
 * @param {Object} args
 * @param {Object} args.user
 * @param {String} args.user._id
 * @return {Promise<Object>}
 */
const updateUser = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context, { _id: args.user._id });

	return Users.findById(args.user._id)
		.then(user => {
			AuthenticateSameUser(context, { _id: user._id });
			const update = isAdmin(context.user.role)
				? args.user
				: { ...args.user, role: user.role };
			Object.assign(user, update);
			return user.save();
		})
		.catch(err => {
			console.log('Problem Updating');
			throw err;
		});
};

/**
 * @function deleteUser
 * @description DELETE deletes a user
 * @param {Object} root - the root resolver object passed (None here since this IS the root)
 * @return {Promise<Object>}
 */
const deleteUser = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context, { isAdminOnly: true });

	return Users.findById(_id)
		.then(user => {
			return user.remove();
		})
		.catch(err => {
			console.error('Error deleting user', err);
			throw err;
		});
};

export default {
	createUser,
	deleteUser,
	updateUser
};
