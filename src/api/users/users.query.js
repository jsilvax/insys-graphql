import Users from './users.model';
import { AuthenticateResolver, AuthenticateSameUser } from '../auth/auth';
import { getUserById } from './users';

/**
 * @function users
 * @description GET users - Only admins can create users & see all users
 * @return {Promise<Array|null>}
 */
const users = (root, args, context, info) => {
	AuthenticateResolver(context, { isAdminOnly: true });
	return Users.find({})
		.then(users => users)
		.catch(err => {
			console.error('Users Error', err);
			return null;
		});
};

/**
 * @function User
 * @description GET a single User
 * @return {Promise<Object|null>}
 */
const user = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);
	return getUserById(_id)
		.then(user => {
			AuthenticateSameUser(context, {
				_id: user._id
			});
			return user;
		})
		.catch(err => {
			console.error('User Error', err);
			return null;
		});
};

export default {
	users,
	user
};
