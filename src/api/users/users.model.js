import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const Schema = mongoose.Schema;

const UserSchema = new Schema({
	username: {
		type: String,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	role: {
		type: String,
		required: true
	},
	email: {
		type: String
	}
});

// This is a middleware function that will run BEFORE the
// user is created or before a user is SAVED.

UserSchema.pre('save', function(next) {
	if (this.isModified('password')) {
		this.password = this.encryptPassword(this.password);
		next();
	} else {
		return next();
	}
});

UserSchema.methods = {
	authenticate(plainTextPassword) {
		// This will check the passwords in log in, make sure they match.
		return bcrypt.compareSync(plainTextPassword, this.password);
	},
	encryptPassword(plainTextPassword) {
		if (!plainTextPassword) return '';

		const salt = bcrypt.genSaltSync(10);
		return bcrypt.hashSync(plainTextPassword, salt);
	},
	toJson() {
		// mongoose returns a document that looks like an object but isnt,
		// this will just convert it so that it is an object
		let obj = this.toObject();
		delete obj.password;
		return obj;
	}
};

export default mongoose.model('user', UserSchema);
