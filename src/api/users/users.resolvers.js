import Query from "./users.query";
import Mutation from "./users.mutation";

export default {
  Query,
  Mutation
};
