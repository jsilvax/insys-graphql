const { exec, mkdir, rm } = require('shelljs');
const path = require('path');
const APP_ROOT = process.cwd();
const MONGO_DATABASE = 'insys-int';
const MONGODUMP_PATH = path.join(APP_ROOT, 'dump');
const BACKUPS_DIR = path.join(APP_ROOT, 'backups');
const BACKUP = 'insys-integration.tgz';
const BACKUP_FOLDER = 'insys-int';

module.exports = () => {
	// Start fresh every time
	exec(`mongo ${MONGO_DATABASE} --eval "db.dropDatabase()"`);
	//  Create the dump dir if it doesnt already exist
	mkdir('-p', MONGODUMP_PATH);
	// Uncompress
	exec(`tar -xvzf ${BACKUPS_DIR}/${BACKUP} -C ${MONGODUMP_PATH}`);
	// Restore
	exec(`mongorestore --db ${MONGO_DATABASE} ${MONGODUMP_PATH}/${BACKUP_FOLDER}`);
	// Remove
	rm('-rf', MONGODUMP_PATH);
};
