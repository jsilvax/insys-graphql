import supertest from 'supertest';
import { app } from '../../server';
import mongoose from 'mongoose';

/**
 * @function setUpLogIn
 * @description a HOF that sets up the logIn query
 * @param {Object} request the supertest request instance
 * @return {Function}
 */
const setUpLogIn = request => (username, password) => {
	const query = {
		query: `
		mutation LOG_IN {
			logIn(username :"${username}", password: "${password}"){
				token
				user {
					username
					role
				}
			}
		}
	`
	};
	return request
		.post('/graphql')
		.set('Accept', 'application/json')
		.expect('Content-Type', /json/)
		.send(query)
		.then(res => {
			const { body, headers } = res;
			if (body.errors) throw body.errors;
			const cookie = headers['set-cookie'][0];
			const tkn = body.data.logIn.token;
			const token = `Bearer ${tkn}`;
			return {
				...body.data.logIn,
				headers,
				cookie,
				token
			};
		});
};

/**
 * @function setUpLogInAdmin
 * @description a HOF that sets up a helper Admin login
 * @param {Object} logIn the logIn function that has been setUp by setUpLogIn
 * @return {Function}
 */
const setUpLogInAdmin = logIn => () => logIn('jsilvax', 'pass1234');

/**
 * @function setUpLogInUser
 * @description a HOF that sets up a helper User login
 * @param {Object} logIn the logIn function that has been setUp by setUpLogIn
 * @return {Function}
 */
const setUpLogInUser = logIn => () => logIn('user1', 'pass2345');

/**
 * @function setUpLogInUser2
 * @description a HOF that sets up a helper User login
 * @param {Object} logIn the logIn function that has been setUp by setUpLogIn
 * @return {Function}
 */
const setUpLogInUser2 = logIn => () => logIn('user2', 'pass3456');
/**
 * @function setUpLogOut
 * @description a HOF that sets up the logOut query
 * @param {Object} request the supertest request instance
 * @return {Function}
 */

const setUpLogOut = request => () => {
	const query = {
		query: `
		mutation LOG_OUT{
			logOut {
				message
			}
		}
	`
	};
	return request
		.post('/graphql')
		.set('Accept', 'application/json')
		.expect('Content-Type', /json/)
		.send(query)
		.then(({ body }) => {
			if (body.errors) throw body.errors;
			return body.data.logOut;
		});
};

let server;
beforeAll(done => {
	server = app.listen('8080', () => {
		console.log('********* Listening *********');
		// Start up the server
		global.server = server;
		const request = supertest.agent(server);
		global.request = request;

		// Set up
		const logIn = setUpLogIn(request);
		global.logIn = logIn;
		global.logInUserTwo = setUpLogInUser2(logIn);
		global.logOut = setUpLogOut(request);
		global.logInAdmin = setUpLogInAdmin(logIn);
		global.logInUser = setUpLogInUser(logIn);
		done();
	});
});

afterAll(done => {
	server.close(() => {
		console.log('********* Closing Server *********');
		mongoose.connection.close();
		done();
	});
});
