import Query from './invoice.query';
import Mutation from './invoice.mutation';
import { user } from '../users/users';

// THE value is the type name defined in .gql
const documentTypeMatcher = {
	INVOICE: 'Invoice',
	PROPOSAL: 'Proposal'
};

export default {
	Query,
	Mutation,
	Document: {
		__resolveType(document) {
			return documentTypeMatcher[document.type.toUpperCase()];
		}
	},
	Invoice: {
		user
	},
	Proposal: {
		user
	}
};
