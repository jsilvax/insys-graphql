import mongoose from 'mongoose';
import paginationPlugin from 'mongoose-paginate-v2';

const Schema = mongoose.Schema;

const ItemSchema = new Schema({
	desc: {
		type: String,
		required: true
	},
	qty: {
		type: Number,
		required: false
	},
	price: {
		type: Number,
		required: false
	}
});

/*
type: invoice || proposal 
*/
const InvoiceSchema = new Schema({
	displayId: {
		type: String,
		unique: true
	},
	items: [ItemSchema],
	total: {
		type: Number,
		required: true
	},
	date: { type: Date, default: Date.now },
	type: {
		type: String,
		default: 'invoice',
		enum: ['invoice', 'proposal']
	},
	paymentStatus: {
		type: String,
		default: 'UNPAID',
		enum: ['PAID', 'UNPAID', 'PENDING'],
		required() {
			return this.type === 'invoice';
		}
	},
	status: { type: String, default: 'DRAFT' },
	title: {
		type: String,
		required: false,
		unique: false
	},
	customer: {
		type: Schema.Types.ObjectId,
		ref: 'customer', // the "user" model defined in customer-model.js
		required: true
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'user', // the "user" model defined in user-model.js
		required: true
	}
});

InvoiceSchema.plugin(paginationPlugin, { limit: 10 });

export default mongoose.model('invoice', InvoiceSchema);
