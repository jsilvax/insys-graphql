const admin = '5c93eb7cc36fa055bcd89e1e';
const user1 = '5c93ebe8287df955cec0090e';
const user2 = '5c93ebf8f1b98355dad27045';

export const allDocs = {
	query: `
		query ALL_INVOICES {
			invoices {
				__typename
				_id
				customer
				user {
					username
				}
			}
		}
	`
};

export const invoices = {
	query: `
		query ALL_INVOICES {
			invoices(type : INVOICE) {
				__typename
				_id
				customer
				user {
					username
				}
			}
		}
	`
};

export const proposals = {
	query: `
		query ALL_INVOICES {
			invoices(type : PROPOSAL) {
				__typename
				_id
				customer
				user {
					username
				}
			}
		}
	`
};

export const invoice = id => ({
	query: `
		query INVOICE {
			invoice(_id : "${id}") {
				__typename
				_id
				customer
				user {
					username
				}
				items {
					price
				}
			}
		}
	`
});

export const createInvoice = (user, customer, desc) => ({
	query: `
		mutation CREATE_INVOICE {
			createInvoice(document : {
				type : INVOICE, 
				user : "${user}",
				customer : "${customer}",
				total : 75,
				items : [{
					desc: "${desc}",
					qty : 1, 
					price : 75
				}]
			}) {
				__typename
				_id
				customer
				user {
					username
				}
				items {
					desc
				}
			}
		}
	`
});
