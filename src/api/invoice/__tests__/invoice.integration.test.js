import {
	allDocs,
	invoices,
	proposals,
	invoice,
	createInvoice
} from './__fixtures__/invoice.queries';
afterEach(done => {
	logOut().then(() => {
		done();
	});
});

describe('GET invoices', () => {
	it('should get ALL documents if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(allDocs)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { invoices } = body.data;
				expect(invoices.length).toBeGreaterThan(0);
				expect(invoices).toMatchSnapshot();
				done();
			});
	});

	it('should get ALL invoices if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(invoices)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { invoices } = body.data;
				expect(invoices.length).toBeGreaterThan(0);
				invoices.forEach(invoice => {
					expect(invoice['__typename']).toEqual('Invoice');
				});

				done();
			});
	});

	it.skip('should get ALL proposals if AUTHORIZED (admin)', async done => {
		// TODO: Set up int db to have proposals
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(proposals)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { invoices } = body.data;
				expect(invoices.length).toBeGreaterThan(0);
				invoices.forEach(invoice => {
					expect(invoice['__typename']).toEqual('Proposals');
				});

				done();
			});
	});

	it('should get documents if AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(allDocs)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { invoices } = body.data;
				invoices.forEach(invoice => {
					expect(invoice.user.username).toEqual('user1');
				});
				done();
			});
	});
});

describe('GET invoice', () => {
	it('should get own invoice if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(invoice('5c93ecc9454e2f55f717cfb3'))
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { invoice } = body.data;
				expect(invoice.items.length).toBeGreaterThan(0);
				done();
			});
	});

	it('should get invoice if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(invoice('5c93eddb454e2f55f717cfc0'))
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { invoice } = body.data;
				expect(invoice.items.length).toBeGreaterThan(0);
				expect(invoice.user.username).toEqual('user1');
				done();
			});
	});

	it('should NOT get an invoice if UNAUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(invoice('5c93ecc9454e2f55f717cfb3'))
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized - You can only access your items');
				done();
			});
	});

	it('should get own invoice if AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(invoice('5c93eddb454e2f55f717cfc0'))
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { invoice } = body.data;
				expect(invoice.items.length).toBeGreaterThan(0);
				expect(invoice.user.username).toEqual('user1');
				done();
			});
	});
});

describe('POST createInvoice', () => {
	it('should create an invoice if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(
				createInvoice(
					'5c93eb7cc36fa055bcd89e1e', // admin ID
					'5c93ec61454e2f55f717cfb0', // customer ID
					'Installed new plugs'
				)
			)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { createInvoice } = body.data;
				expect(createInvoice.items[0].desc).toEqual('Installed new plugs');
				expect(createInvoice.user.username).toEqual('jsilvax');
				expect(createInvoice.customer).toEqual('5c93ec61454e2f55f717cfb0');
				done();
			});
	});

	it('should NOT create an invoice if UNAUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(
				createInvoice(
					'5c93eb7cc36fa055bcd89e1e', // admin ID
					'5c93ec61454e2f55f717cfb0', // customer ID
					'Installed new plugs'
				)
			)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized - You can only access your items');
				done();
			});
	});

	it('should create an invoice if AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(
				createInvoice(
					'5c93ebe8287df955cec0090e', // user1 ID
					'5c93ed79454e2f55f717cfbc', // customer ID
					'Did Something'
				)
			)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const { createInvoice } = body.data;
				expect(createInvoice.items[0].desc).toEqual('Did Something');
				expect(createInvoice.user.username).toEqual('user1');
				expect(createInvoice.customer).toEqual('5c93ed79454e2f55f717cfbc');
				done();
			});
	});
});
