import Invoice from './invoice.model';
import { AuthenticateResolver, AuthenticateSameUser, isAdmin } from '../auth/auth';
import logger from '@/logging/logger';

const getUserFilterQuery = (context, user) => {
	const _id = isAdmin(context.user.role) && user ? user : context.user._id;
	return isAdmin(context.user.role) && !user ? {} : { user: _id };
};

/**
 * @function invoices
 * @description GET invoices
 * @param {Object} root
 * @param {Object} args
 * @param {String} [args.user] The user _id
 * @param {String} [args.type] The document type "INVOICE" || "PROPOSAL"
 * @param {String} [args.where] pagination options
 * @return {Promise<Array|null>}
 */
const invoices = (root, { type, ...args }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	const user = getUserFilterQuery(context, args.user);
	const query = type ? { type: type.toLowerCase(), ...user } : { ...user };

	return Invoice.paginate(query, args.where)
		.then(invoices => invoices.docs)
		.catch(err => {
			logger.error('Invoices Error', err);
			return null;
		});
};

/**
 * @function Invoice
 * @description GET a single Invoice
 * @param {Object} root
 * @param {Object} args
 * @param {String} args._id The invoice _id
 * @return {Promise<Object|null>}
 */
const invoice = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Invoice.findById(_id)
		.then(invoice => {
			if (!invoice) throw new Error('No Invoice with that id');
			AuthenticateSameUser(context, { _id: invoice.user });
			return invoice;
		})
		.catch(err => {
			logger.error('Invoice Error', err);
			throw err;
		});
};

export default {
	invoices,
	invoice
};
