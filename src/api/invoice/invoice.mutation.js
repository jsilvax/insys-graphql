import Invoice from './invoice.model';
import { AuthenticateResolver, isAdmin, AuthenticateSameUser } from '../auth/auth';
import logger from '@/logging/logger';
import { getCustomerById } from '../customers/customers';
// TODO: export this from a helper file
const getDocumentUser = (context, model) => {
	return isAdmin(context.user.role) && model.user ? model.user : context.user._id;
};

/**
 * @function createInvoice
 * @description POST Create a new invoice document in mongo
 * @param {Object} context
 * @param {Boolean} context.isAuthenticated
 * @param {Object} context.user The logged in user
 * @param {String} context.user._id The logged in user
 * @param {String} context.user.role The logged in user _id
 * @return {Promise<Object>}
 */
const createInvoice = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context, { _id: args.document.user });

	// Make sure we only save to the owner (user)
	const user = getDocumentUser(context, args.document);
	const invoice = { ...args.document, user };

	// TODO: Change all DB entries to uppercase enum
	invoice.type = invoice.type.toLowerCase();
	return getCustomerById(invoice.customer)
		.then(customer => {
			if (customer.user.toString() !== user.toString() && !isAdmin(context.user.role)) {
				throw new Error('Unauthorized, that is not your customer');
			}
		})
		.then(() => Invoice.create(invoice))
		.then(invoice => invoice)
		.catch(err => {
			logger.error('Error creating new Invoice', err);
			throw err;
		});
};

/**
 * @function updateInvoice
 * @description PUT Update an invoice document in mongo
 * @param {Object} root
 * @param {Object} args
 * @param {Object} args.document
 * @param {String} args.document._id
 * @param {String} args.user The user _id
 * @return {Promise<Object>}
 */
const updateInvoice = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Invoice.findById(args.document._id)
		.then(invoice => {
			// Make sure we only save to the owner
			const user = getDocumentUser(context, invoice);
			const update = { ...args.document, user };
			Object.assign(invoice, update);
			return invoice.save();
		})
		.catch(err => {
			logger.warn('Problem Updating Invoice %s', err);
			throw err;
		});
};

/**
 * @function deleteInvoice
 * @description DELETE deletes a invoice
 * @param {Object} root - the root resolver object passed (None here since this IS the root)
 * @return {Promise<Object>}
 */
const deleteInvoice = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Invoice.findById(_id)
		.then(invoice => {
			// Verify its the same user
			AuthenticateSameUser(context, {
				_id: invoice.user
			});

			return invoice.remove();
		})
		.catch(err => {
			logger.error('Error deleting invoice', err);
			throw err;
		});
};

export default {
	createInvoice,
	deleteInvoice,
	updateInvoice
};
