import Query from './properties.query';
import Mutation from './properties.mutation';
import { user } from '../users/users';
import { customer } from '../customers/customers';

export default {
	Query,
	Mutation,
	Property: {
		user,
		customer
	}
};
