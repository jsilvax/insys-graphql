import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const PropertiesSchema = new Schema({
	propertyName: {
		type: String
	},
	address: {
		type: String
	},
	city: {
		type: String
	},
	state: {
		type: String
	},
	zip: {
		type: String
	},
	email: {
		type: String
	},
	phone: {
		type: String
	},
	customer: {
		type: Schema.Types.ObjectId,
		ref: 'customer', // the "customer" model defined in customer.model.js
		required: true
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'user', // the "user" model defined in user.model.js
		required: true
	}
});

export default mongoose.model('property', PropertiesSchema);
