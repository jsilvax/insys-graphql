import Property from './properties.model';
import { AuthenticateResolver, AuthenticateSameUser, isAdmin } from '../auth/auth';

/**
 * @function createProperty
 * @description POST Create a new property document in mongo
 * @param {Object} args
 * @param {Object} args.property
 * @param {Object} context
 * @return {Promise<Object>}
 */
const createProperty = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);
	const newProperty = args.property;
	// Make sure we only save to the user logged in
	newProperty.user = isAdmin(context.user.role) ? newProperty.user : context.user._id;
	return Property.create(newProperty)
		.then(property => property)
		.catch(err => {
			console.error('Error creating new Property', err);
			throw err;
		});
};

/**
 * @function updateProperty
 * @description PUT Update an property in mongo
 * @param {Object} root
 * @param {Object} args
 * @param {Object} args.property
 * @param {String} args.property._id
 * @param {String} args.user The user _id
 * @return {Promise<Object>}
 */
const updateProperty = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Property.findById(args.property._id)
		.then(property => {
			// Make sure we only save to the user logged in
			const user = isAdmin(context.user.role) ? property.user : context.user._id;
			const update = { ...args.property, user };
			Object.assign(property, update);
			return property.save();
		})
		.catch(err => {
			console.log('Problem Updating Property');
			throw err;
		});
};

/**
 * @function deleteProperty
 * @description DELETE deletes a property
 * @param {Object} root - the root resolver object passed (None here since this IS the root)
 * @return {Promise<Object>}
 */
const deleteProperty = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context, {});

	return Property.findById(_id)
		.then(property => {
			// Check if its the same user otherwise will throw
			AuthenticateSameUser(context, { _id: property.user._id });
			return property.remove();
		})
		.catch(err => {
			console.error('Error deleting property', err);
			throw err;
		});
};

export default {
	createProperty,
	deleteProperty,
	updateProperty
};
