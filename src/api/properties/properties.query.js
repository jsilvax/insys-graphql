import Property from './properties.model';
import { AuthenticateResolver, AuthenticateSameUser } from '../auth/auth';

/**
 * @function properties
 * @description GET properties
 * @param {Object} root
 * @param {Object} args
 * @param {String} [args.customer] The customer _id
 * @param {String} [args.user] The user _id
 * @param {Object} context
 * @param {Object} context.user
 * @param {String} context.user._id
 * @return {Promise<Array|null>}
 */
const properties = (root, { customer }, context, info) => {
	// TODO: implement args.user logic
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	const { user } = context;
	const query = customer ? { user: user._id, customer } : { user: user._id };

	return Property.find(query)
		.then(properties => properties)
		.catch(err => {
			console.error('Properties Error', err);
			return null;
		});
};

/**
 * @function Property
 * @description GET a single Property
 * @param {Object} root
 * @param {Object} args
 * @param {String} args._id The property _id
 * @return {Promise<Object|null>}
 */
const property = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Property.findById(_id)
		.then(property => {
			if (!property) throw new Error('No Property with that id');
			// Check if its the same user otherwise will throw
			AuthenticateSameUser(context, {
				_id: property.user._id
			});
			return property;
		})
		.catch(err => {
			console.error('Property Error', err);
			return null;
		});
};

export default {
	properties,
	property
};
