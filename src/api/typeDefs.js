import schema from './schema.gql';
import company from './company/company.gql';
import customers from './customers/customers.gql';
import invoice from './invoice/invoice.gql';
import users from './users/users.gql';
import auth from './auth/auth.gql';
import properties from './properties/properties.gql';

export default [schema, company, customers, invoice, users, auth, properties];
