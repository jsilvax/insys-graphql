import Customer from './customers.model';

/**
 * @function getCustomerById
 * @description The interface resolver helper func
 * @param {String} _id
 * @return {Promise<Object>}
 */
export const getCustomerById = _id => {
	return Customer.findById(_id).then(customer => {
		if (!customer) throw new Error('No Customer with that id');
		return customer;
	});
};

/**
 * @function customer
 * @description The interface resolver helper func
 * @param {Object} root - The root object passed from the resolver
 * @param {String} root.customer - The user _id
 * @return {Promise<Object>}
 */

export const customer = root => {
	return getCustomerById(root.customer).catch(() => null);
};
