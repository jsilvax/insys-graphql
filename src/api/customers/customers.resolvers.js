import Query from './customers.query';
import Mutation from './customers.mutation';
import { user } from '../users/users';

export default {
	Query,
	Mutation,
	Customer: {
		user
	}
};
