import mongoose from 'mongoose';
import paginationPlugin from 'mongoose-paginate-v2';

const Schema = mongoose.Schema;

const CustomerSchema = new Schema({
	name: {
		first: {
			type: String,
			required: true
		},
		middle: {
			type: String
		},
		last: {
			type: String,
			required: true
		}
	},
	address: {
		type: String
	},
	city: {
		type: String
	},
	state: {
		type: String
	},
	zip: {
		type: String
	},
	email: {
		type: String
	},
	phone: {
		type: String
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'user', // the "user" model defined in user-model.js
		required: true
	}
});

CustomerSchema.methods = {
	toJson: function() {
		// mongoose returns a document that looks like an object but isnt,
		// this will just convert it so that it is an object
		return this.toObject();
	}
};

CustomerSchema.plugin(paginationPlugin, { limit: 10 });

export default mongoose.model('customer', CustomerSchema);
