const admin = '5c93eb7cc36fa055bcd89e1e';
const user1 = '5c93ebe8287df955cec0090e';
const user2 = '5c93ebf8f1b98355dad27045';

export const adminsCustomers = {
	query: `
		query GET_CUSTOMERS {
			customers(user : "${admin}") {
				name {
					first
				}
				user {
					username
				}
			}
		}
	`
};

export const customersQuery = {
	query: `
		query GET_ALL_CUSTOMERS {
			customers {
				name {
					first
				}
				user {
					username
				}
			}
		}
	`
};

export const singleQuery = {
	query: `
		query GET_CUSTOMER {
			customer(_id: "5c93edb8454e2f55f717cfbf") {
				name {
					first
				}
				user {
					username
				}
			}
		}
	`
};

export const createCustomerQuery = {
	query: `
		mutation CREATE_CUSTOMER {
			createCustomer(customer: {
        name : { 
          first : "Snicky", 
          last : "Silva-Walters"
        }
      }) {
		  _id
				name {
          first
          last
        }
				user {
					username
				}
			}
		}
	`
};

export const createCustomerWfakeUserQuery = {
	query: `
		mutation CREATE_CUSTOMER {
			createCustomer(customer: {
        name : { 
          first : "Snicky", 
          last : "Silva-Walters"
		}
		user : "5bd3f066e7b78772f7dd4917"
      }) {
		  _id
		name {
          first
          last
        }
				user {
					_id
					username
				}
			}
		}
	`
};

export const createCustomerWUserQuery = {
	query: `
		mutation CREATE_CUSTOMER {
			createCustomer(customer: {
        name : { 
          first : "Snicky", 
          last : "Silva-Walters"
		}
		user : "${user2}"
      }) {
		  _id
		name {
          first
          last
        }
				user {
					_id
					username
				}
			}
		}
	`
};

// User1 is the owner of this customer
export const updateCustomer = {
	query: `
		mutation UPDATE_CUSTOMER {
			updateCustomer(customer: {
				_id : "5c93ed79454e2f55f717cfbc",
        name : {
          first : "Snicks", 
          last : "Sykes"
				}
      }) {
		  _id
			name {
          first
          last
        }
				user {
					_id
					username
				}
			}
		}
	`
};

// User1 is the owner of this customer
export const updateCustomerwAdmin = {
	query: `
		mutation UPDATE_CUSTOMER {
			updateCustomer(customer: {
				_id : "5c93ed79454e2f55f717cfbc",
        name : {
          first : "Snickaroo", 
          last : "Silva"
				}
      }) {
		  _id
			name {
          first
          last
        }
				user {
					_id
					username
				}
			}
		}
	`
};

export const deleteCustomer = {
	query: `
		mutation DELETE_CUSTOMER {
			deleteCustomer(
				_id : "5c93ed79454e2f55f717cfbc"
      ) {
		  	_id
				name {
          first
          last
        }
				user {
					_id
					username
				}
			}
		}
	`
};
// jsilvax
export const deleteADMINCustomer = {
	query: `
		mutation DELETE_CUSTOMER {
			deleteCustomer(
				_id : "5c93ec61454e2f55f717cfb0"
      ) {
		  	_id
				name {
          first
          last
        }
				user {
					_id
					username
				}
			}
		}
	`
};

// user1 owns
export const deleteOtherCustomerAdmin = {
	query: `
		mutation DELETE_CUSTOMER {
			deleteCustomer(
				_id : "5c93edb8454e2f55f717cfbf"
      ) {
		  	_id
				name {
          first
          last
        }
				user {
					_id
					username
				}
			}
		}
	`
};
