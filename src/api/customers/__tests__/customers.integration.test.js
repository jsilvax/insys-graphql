import {
	customersQuery,
	adminsCustomers,
	singleQuery,
	createCustomerQuery,
	createCustomerWfakeUserQuery,
	createCustomerWUserQuery,
	updateCustomer,
	updateCustomerwAdmin,
	deleteCustomer,
	deleteADMINCustomer,
	deleteOtherCustomerAdmin
} from './__fixtures__/customers.queries';

afterEach(done => {
	logOut().then(() => {
		done();
	});
});

describe('GET customers', () => {
	it('should get ALL customers if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(customersQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const {
					data: { customers }
				} = body;
				expect(customers.length).toBeGreaterThan(0);

				done();
			});
	});
	it('should get admins customers if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(adminsCustomers)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const {
					data: { customers }
				} = body;
				expect(customers.length).toBeGreaterThan(0);
				customers.forEach(customer => {
					expect(customer.user.username).toEqual('jsilvax');
				});
				done();
			});
	});

	it('should get customers if AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(customersQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const customers = body.data.customers;
				expect(customers.length).toBeGreaterThan(0);

				// Should only be items for this user
				customers.forEach(({ user }) => {
					expect(user.username).toEqual('user1');
				});

				done();
			});
	});

	it('should NOT get customers if UNAUTHORIZED', async done => {
		return request
			.post('/graphql')
			.send(customersQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized, please log in');
				done();
			});
	});
});

describe('GET customer', () => {
	it('should get a customer if AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(singleQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const {
					data: { customer }
				} = body;
				expect(customer.user.username).toEqual('user1');
				done();
			});
	});

	it('should get a users customer if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(singleQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const {
					data: { customer }
				} = body;
				expect(customer.user.username).toEqual('user1');
				done();
			});
	});
});

describe('POST createCustomer', () => {
	it('should create a customer in that users account', async done => {
		const { cookie, token } = await logInUser();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(createCustomerQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const customer = body.data.createCustomer;
				expect(customer.user.username).toEqual('user1');
				expect(customer.name.first).toEqual('Snicky');
				expect(customer.name.last).toEqual('Silva-Walters');
				done();
			});
	});

	it('should create customer under user even when they pass a diff id', async done => {
		const { cookie, token } = await logInUser();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(createCustomerWfakeUserQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const customer = body.data.createCustomer;
				expect(customer.user._id).not.toEqual('5bd3f066e7b78772f7dd4917');
				expect(customer.user.username).toEqual('user1');
				expect(customer.name.first).toEqual('Snicky');
				expect(customer.name.last).toEqual('Silva-Walters');
				done();
			});
	});

	it("should create a customer in ADMIN's account", async done => {
		const { cookie, token } = await logInAdmin();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(createCustomerQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const customer = body.data.createCustomer;
				expect(customer.user.username).toEqual('jsilvax');
				expect(customer.name.first).toEqual('Snicky');
				expect(customer.name.last).toEqual('Silva-Walters');
				done();
			});
	});

	it('should create a customer with provided user account (ADMIN)', async done => {
		const { cookie, token } = await logInAdmin();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(createCustomerWUserQuery)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const customer = body.data.createCustomer;
				expect(customer.user.username).toEqual('user2');
				done();
			});
	});
});

describe('UPDATE updateCustomer', () => {
	// TODO: Test updates to properties. Adding multiple, and deleting one of them
	it('should update a customer if AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(updateCustomer)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const customer = body.data.updateCustomer;
				expect(customer.user.username).toEqual('user1');
				expect(customer.name.first).toEqual('Snicks');
				expect(customer.name.last).toEqual('Sykes');
				done();
			});
	});

	it('should update a customer if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(updateCustomerwAdmin)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
				const customer = body.data.updateCustomer;
				expect(customer.user.username).toEqual('user1');
				expect(customer.name.first).toEqual('Snickaroo');
				expect(customer.name.last).toEqual('Silva');
				done();
			});
	});

	it('should NOT update a customer if NOT AUTHORIZED', async done => {
		const { cookie, token } = await logInUserTwo();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(updateCustomer)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized - You can only access your items');
				done();
			});
	});
});

describe('DELETE deleteCustomer', () => {
	it('should NOT delete if NOT AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUserTwo();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(deleteCustomer)
			.then(({ body }) => {
				if (body.errors) throw body.errors;
			})
			.catch(err => {
				expect(err[0].message).toEqual('Unauthorized - You can only access your items');
				done();
			});
	});
	it('should delete if AUTHORIZED (user)', async done => {
		const { cookie, token } = await logInUser();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(deleteCustomer)
			.then(({ body }) => {
				expect(body.data.deleteCustomer).toBeDefined();
				expect(body.data.deleteCustomer.user.username).toEqual('user1');
				done();
			});
	});

	it('should delete if AUTHORIZED & own (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(deleteADMINCustomer)
			.then(({ body }) => {
				expect(body.data.deleteCustomer).toBeDefined();
				expect(body.data.deleteCustomer.user.username).toEqual('jsilvax');
				done();
			});
	});

	it('should delete if AUTHORIZED (admin)', async done => {
		const { cookie, token } = await logInAdmin();
		return request
			.post('/graphql')
			.set('Cookie', cookie)
			.set('Authorization', token)
			.send(deleteOtherCustomerAdmin)
			.then(({ body }) => {
				expect(body.data.deleteCustomer).toBeDefined();
				expect(body.data.deleteCustomer.user.username).toEqual('user1');
				done();
			});
	});
});
