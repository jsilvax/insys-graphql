import Customer from './customers.model';
import { AuthenticateResolver, AuthenticateSameUser, isAdmin } from '../auth/auth';
import { getCustomerById } from './customers';

// TODO: Export as a helper func
const getUserFilterQuery = (context, user) => {
	const _id = isAdmin(context.user.role) && user ? user : context.user._id;
	return isAdmin(context.user.role) && !user ? {} : { user: _id };
};

/**
 * @function customers
 * @description GET customers
 * @return {Promise<Array|null>}
 */
const customers = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);
	const query = getUserFilterQuery(context, args.user);
	// Only get customers for user who is logged in
	return Customer.paginate(query, args.where)
		.then(customers => customers.docs)
		.catch(err => {
			console.error('Customers Error', err);
			return null;
		});
};

/**
 * @function customer
 * @description GET a single Customer
 * @return {Promise<Object|null>}
 */
const customer = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return getCustomerById(_id)
		.then(customer => {
			AuthenticateSameUser(context, { _id: customer.user });
			return customer;
		})
		.catch(err => {
			console.error('Customer Error', err);
			return null;
		});
};

export default {
	customers,
	customer
};
