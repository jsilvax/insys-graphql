import Customer from './customers.model';
import { AuthenticateResolver, isAdmin, AuthenticateSameUser } from '../auth/auth';

const getDocumentUser = (context, model) => {
	return isAdmin(context.user.role) && model.user ? model.user : context.user._id;
};
/**
 * @function createCustomer
 * @description POST Create a new customer document in mongo
 * @return {Promise<Object>}
 */
const createCustomer = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	// Make sure we only save to the owner (user)
	const user = getDocumentUser(context, args.customer);

	const customer = { ...args.customer, user };

	return Customer.create(customer)
		.then(customer => customer)
		.catch(err => {
			console.error('Error creating new Customer', err);
			throw err;
		});
};

/**
 * @function updateCustomer
 * @description PUT Update an customer document in mongo
 * @param {Object} root
 * @param {Object} args
 * @param {Object} args.customer
 * @param {String} args.customer._id
 * @param {String} args.user The user _id
 * @return {Promise<Object>}
 */
const updateCustomer = (root, args, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Customer.findById(args.customer._id)
		.then(customer => {
			AuthenticateSameUser(context, { _id: customer.user });
			// Make sure we only save to the owner
			const user = getDocumentUser(context, customer);
			const update = { ...args.customer, user };
			Object.assign(customer, update);
			return customer.save();
		})
		.catch(err => {
			console.log('Problem Updating Customer', err.message);
			throw err;
		});
};

/**
 * @function deleteCustomer
 * @description DELETE deletes a customer
 * @param {Object} root - the root resolver object passed (None here since this IS the root)
 * @return {Promise<Object>}
 */
const deleteCustomer = (root, { _id }, context, info) => {
	// Query level authentication - throws if Authentication fails
	AuthenticateResolver(context);

	return Customer.findById(_id)
		.then(customer => {
			// Verify its the same user
			AuthenticateSameUser(context, {
				_id: customer.user
			});

			return customer.remove();
		})
		.catch(err => {
			console.error('Error deleting customer', err);
			throw err;
		});
};

export default {
	createCustomer,
	deleteCustomer,
	updateCustomer
};
