import { createLogger, format, transports } from 'winston';
import 'winston-daily-rotate-file';

const { combine, timestamp, printf } = format;

/**
 * @function formatter
 * @description Formats winston log files
 * @return {Function}
 */
const formatter = printf(
	({ timestamp, level, message, stack }) => `${timestamp} ${level}: ${message} ${stack || ''}`
);

/**
 * @function consoleFormatter
 * @description Formats winston logs in the console
 * @return {Function}
 */
const consoleFormatter = printf(
	({ level, message, stack }) => `${level}: ${message} ${stack || ''}`
);

/**
 * @function setUpStream
 * @description Sets up a stream object with a 'write' function that will be used by `morgan`.
 * This stream is based on node.js stream https://nodejs.org/api/stream.html.
 * @return {Object}
 */
const setUpStream = logger => ({
	write(message, encoding) {
		logger.info(message);
	}
});

/**
 * @function enumerateErrorFormat
 * @description formats errors - Code from issue: https://github.com/winstonjs/winston/issues/1338
 */

const enumerateErrorFormat = format(info => {
	if (info.message instanceof Error) {
		info.message = Object.assign(
			{
				message: info.message.message,
				stack: info.message.stack
			},
			info.message
		);
	}

	if (info instanceof Error) {
		return Object.assign(
			{
				message: info.message,
				stack: info.stack
			},
			info
		);
	}

	return info;
});

/**
 * @function startLogger
 * @description Creates an instance of Winston Logger
 * @param {Object} config - the app configuration
 * @param {String} config.logLevel - The log level ex. info || debug
 * @returns {winston.Logger}
 */
export default ({ logLevel }) => {
	const logger = createLogger({
		level: logLevel,
		transports: [
			new transports.DailyRotateFile({
				filename: 'logs/combined-%DATE%.log',
				datePattern: 'YYYY-MM-DD',
				maxSize: '20m', // 20 MB
				maxFiles: '15d', // Keep files for 15 days
				handleExceptions: true
			})
		],
		format: combine(
			enumerateErrorFormat(),
			format.json(),
			format.splat(),
			timestamp({
				format: 'YYYY-MM-DD:HH:mm:ss'
			}),
			formatter
		),
		exitOnError: false
	});
	// stream - compatible with morgan
	logger.stream = setUpStream(logger);

	if (process.env.NODE_ENV !== 'production') {
		logger.clear().add(
			new transports.Console({
				handleExceptions: true,
				format: combine(
					enumerateErrorFormat(),
					format.json(),
					format.splat(),
					format.colorize(),
					consoleFormatter
				)
			})
		);
	}

	return logger;
};
