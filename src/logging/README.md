## Usage

```js
// Initialize the winston logger
const logger = winston({ logLevel: LOG_LEVEL });

// Set it in the express server/app instance 
app.set('logger', logger);

// Set up morgan logging to be compatible with winston by passing the winston logger stream
app.use(morgan({ stream: logger.stream }));
```
And to differentiate between LOCAL and DEPLOYED environment you'll need to set an IS_LOCAL environment variable. Ex. 
```js
"scripts" : {
    "start": "export IS_LOCAL=true pm2 start ecosystem.config.js --env dev --no-daemon",
    "dev": "export IS_LOCAL=true pm2 start ecosystem.config.js --env dev --no-daemon",
}
```
### To add additional logging 
Example: 
```js
// Middleware to validate the request is a supported edition
router.param('edition', (req, res, next) => {
    // Get the logger instance 
    const logger = req.app.get('logger');
    
    // Log something 
    logger.debug('Validating edition');
	
	if (isValidEdition(req.params.edition)) {
		next();
	} else {
		logger.verbose(`Requesting invalid edition - ${req.params.edition}`);
		FourOhFourController(req, res);
	}
});
```
### Logging Levels
Winston's logging levels are as follows:
```js 
const levels = { 
  error: 0, 
  warn: 1, 
  info: 2, 
  verbose: 3, 
  debug: 4, 
  silly: 5 
};
```
You may use any to indicate the logging level. Ex. 
```js
const logger = req.app.get('logger');
logger.error(new Error("Something went terribly wrong"));
logger.warn("GraphQL server is down, sending user to a STATIC 404 page");
logger.info("User requested royals category");
logger.verbose("Requesting invalid edition");
logger.debug("Calling await getTranslations()");
logger.silly("Calling helmet");
```

### File Rotation 
To rotate logs we use the following package: 
https://www.npmjs.com/package/winston-daily-rotate-file

This is set to rotate files every `15 days` and a new log file is created every day. 