import winston from '@/logging/winston';
import { LOG_LEVEL } from '@/config/config';

// Initialize the winston logger
export default winston({ logLevel: LOG_LEVEL });
