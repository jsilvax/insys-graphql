import morgan from 'morgan';

/**
 * @const formatter
 * @description A standardized default format for morgan
 */
const formatter = `:method :url status=:status - :response-time ms`;

/**
 * @function morgan
 * @description A standardized morgan logger
 * @param {Object} options
 * @param {Object} [options.stream]
 */

export default (options = {}) => {
	// Stream options are only allowed in a deployed env
	const opt = process.env.NODE_ENV !== 'production' ? null : options;
	return morgan(formatter, opt);
};
