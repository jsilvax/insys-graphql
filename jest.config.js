module.exports = {
	testEnvironment: 'node',
	testPathIgnorePatterns: ['/node_modules/', '/__utils__/', 'integration', '/__fixtures__/'],
	transform: {
		'\\.(gql|graphql)$': 'jest-transform-graphql',
		'.*': 'babel-jest'
	},
	moduleNameMapper: {
		'^@/(.*)$': '<rootDir>/src/$1'
	}
};
