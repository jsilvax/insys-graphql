import { app } from './src/server';
import { PORT } from './src/config/config';
import logger from '@/logging/logger';

app.listen(PORT, () => {
	console.log(`🚀 Server listening on port: ${PORT}`);
	logger.info(`🚀 Server listening on port: ${PORT}`);
});
