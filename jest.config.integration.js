// Jest config for integration tests

module.exports = {
	testEnvironment: 'node',
	setupFilesAfterEnv: ['<rootDir>/src/api/__utils__/integration.setup.js'],
	testPathIgnorePatterns: ['/node_modules/', '/__utils__/'],
	testRegex: 'integration\\.test\\.[jt]sx?$',
	transform: {
		'\\.(gql)$': 'jest-transform-graphql',
		'.*': 'babel-jest'
	},
	globalSetup: '<rootDir>/src/api/__utils__/integration.global.setup.js',
	verbose: true,
	moduleNameMapper: {
		'^@/(.*)$': '<rootDir>/src/$1'
	}
};
