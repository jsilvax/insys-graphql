const nodeExternals = require('webpack-node-externals');
const path = require('path');
const NODE_ENV = process.env.NODE_ENV || 'development';
const IS_PROD = process.env.NODE_ENV === 'production';

module.exports = {
	entry: {
		app: './index.js'
	},
	output: {
		filename: 'index.js',
		path: path.join(__dirname, '/dist')
	},
	mode: NODE_ENV,
	target: 'node',
	bail: IS_PROD,
	module: {
		rules: [
			{
				test: /\.(graphql|gql)$/,
				exclude: /node_modules/,
				use: 'graphql-tag/loader'
			},
			{
				test: /\.(js)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							configFile: path.join(__dirname, 'babel.config.js')
						}
					}
				]
			},
			{
				test: /\.mjs$/,
				exclude: /node_modules/,
				type: 'javascript/auto'
			}
		]
	},
	optimization: {
		nodeEnv: false
	},
	resolve: {
		modules: ['node_modules'],
		extensions: ['.js', '.gql', '.graphql', '.json', '.mjs'],
		alias: {
			'@': path.join(__dirname, 'src')
		}
	},
	node: {
		fs: 'empty',
		net: 'empty',
		tls: 'empty'
	},
	externals: [nodeExternals()]
};
