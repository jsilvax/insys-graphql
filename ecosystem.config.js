// Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
// This config is for switching environments. (Local & Bamboo)
// DO NOT ADD SENSITIVE INFO IN THIS FILE

const IS_PROD = process.env.NODE_ENV === 'production';

module.exports = {
	name: 'insys-graphql',
	script: './dist/index.js',
	watch: IS_PROD ? false : ['./dist/index.js'],
	autorestart: !IS_PROD,
	env_production: {
		NODE_ENV: 'production',
		LOG_LEVEL: 'info'
	},
	env_dev: {
		NODE_ENV: 'development',
		LOG_LEVEL: 'info'
	}
};
